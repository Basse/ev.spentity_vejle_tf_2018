﻿using System;
using System.IO;
using System.Net.Mail;
using Microsoft.SharePoint.Administration;

namespace EV.SPEntityService
{
    internal class EMailMessage
    {
        internal int Port { get; set; }

        internal string Server { get; set; }

        private readonly MailMessage _msg;
        private readonly SmtpClient _client;


        internal EMailMessage(string fromtext)
        {
            _msg = new MailMessage
                       {
                           IsBodyHtml = false,
                           From =
                               new MailAddress(SPAdministrationWebApplication.Local.OutboundMailSenderAddress,
                                               fromtext)
                       };

            if (string.IsNullOrEmpty(SPAdministrationWebApplication.Local.OutboundMailServiceInstance.Server.Address))
                throw new Exception("Smtp-server er ikke defineret i sharepoint");

            _client = new SmtpClient(SPAdministrationWebApplication.Local.OutboundMailServiceInstance.Server.Address);    
        }

        internal void AddRecipient(string email)
        {
            _msg.To.Add(email);
            //_msg.To.Add("sen@elbek-vejrup.dk");
        }

        internal void AddCc(string email)
        {
            _msg.CC.Add(email);
        }

        internal void AddAttachment(Stream stream, string name)
        {
            _msg.Attachments.Add(new Attachment(stream, name));
            //var a = new Attachment(
        }

        internal void AddAttachment(byte[] stream, string name)
        {
            var ms = new MemoryStream(stream);
            _msg.Attachments.Add(new Attachment(ms, name));
            //var a = new Attachment(
        }

        public string Subject
        {
            get
            {
                return _msg.Subject;
            }
            set { _msg.Subject = value; }
        }

        public string Body
        {
            get
            {
                return _msg.Body;
            }
            set { _msg.Body = value; }
        }

        internal void SendMessage()
        {
            _client.Send(_msg);
        }

    }
}
