﻿using System.ServiceProcess;

namespace EV.SPEntityService
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            var servicesToRun = new ServiceBase[] 
                                              { 
                                                  new EntityService() 
                                              };
            ServiceBase.Run(servicesToRun);
        }
    }
}
