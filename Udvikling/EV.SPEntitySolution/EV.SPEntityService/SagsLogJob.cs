﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Quartz;
using EV.SPEntity;

namespace EV.SPEntityService
{
    
    public class SagsLogJob : IStatefulJob
    {
        private string _rooturl;
        private string _wsurl;
        private string _company;

        public void Execute(JobExecutionContext context)
        {
            _wsurl = context.JobDetail.JobDataMap.Get("wsurl") as string;
            _rooturl = context.JobDetail.JobDataMap.Get("rooturl") as string;     
            _company = context.JobDetail.JobDataMap.Get("company") as string;

            ProcessLog();
        }

        public void DebugExecute()
        {
            _wsurl = "http://evudv-sql1:7047/DynamicsNAV/WS/JORTON_A_S_280909/";
            _rooturl = "http://evudv-devpc-prh/";
            _company = "JORTON";

            ProcessLog();

        }

        private void ProcessLog()
        {
            try
            {
                //Debug ..

                //Get LogItems
                var nData = new NavEntityData(_wsurl);
                var pages = nData.GetLogitems(25);

                foreach (var page in pages)
                {
                    switch (page.Tabelnr)
                    {
                        case 167:
                            switch (page.Opdateringstype)
                            {
                                case SPEntity.SagsLogService.Opdateringstype.Insert:
                                    CreateSite(page);
                                    break;
                                case SPEntity.SagsLogService.Opdateringstype.Update:
                                    UpdateSite(page);
                                    break;
                                case SPEntity.SagsLogService.Opdateringstype.Delete:
                                    DeleteSite(page);
                                    break;
                                case SPEntity.SagsLogService.Opdateringstype.Rename:
                                    UpdateSite(page);
                                    break;
                            }
                            break;
                        case 80002:
                            switch (page.Opdateringstype)
                            {
                                case SPEntity.SagsLogService.Opdateringstype.Insert:
                                    AddUser(page);
                                    break;
                                case SPEntity.SagsLogService.Opdateringstype.Update:
                                    UpdateUser(page);
                                    break;
                                case SPEntity.SagsLogService.Opdateringstype.Delete:
                                    RemoveUser(page);
                                    break;
                                case SPEntity.SagsLogService.Opdateringstype.Rename:
                                    break;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToEventLog(ex.Message, EventLogEntryType.Error);
            }
        }

        #region adgangslog

        private void AddUser(SPEntity.SagsLogService.Misc_Sagslog page)
        {
            try
            {
                if (!string.IsNullOrEmpty(page.BrugerId) && !string.IsNullOrEmpty(page.URLGruppe))
                {                    
                    string weburl = _rooturl.TrimEnd('/') + page.SPUrl;
                    using (var web = new SPEntityWeb(weburl))
                    {
                        web.AddUser(page.BrugerId, NavEntityData.NavRolleTilSpRolle(page.SPRolle));
                    }
                    CreateFeedbackLog($"Brugeren '{page.BrugerId}' er blevet tilføjet sitet.", page.Lbnr, null);
                }
                CreateLoganswer(page, false);
            }
            catch (Exception ex)
            {
                CreateFeedbackLog($"Brugeren '{page.BrugerId}' blev ikke tilføjet sitet.", page.Lbnr, ex);
                CreateLoganswer(page, true);
            }
        }


        private void UpdateUser(SPEntity.SagsLogService.Misc_Sagslog page)
        {
            try
            {
                if (!string.IsNullOrEmpty(page.BrugerId) && !string.IsNullOrEmpty(page.URLGruppe))
                {
                    string weburl = _rooturl.TrimEnd('/') + page.SPUrl;
                    using (var web = new SPEntityWeb(weburl))
                    {
                        web.RemoveUser(page.BrugerId);
                        web.AddUser(page.BrugerId, NavEntityData.NavRolleTilSpRolle(page.SPRolle));
                    }
                    CreateFeedbackLog($"Brugeren '{page.BrugerId}' rettigheder blev opdateret", page.Lbnr, null);
                }
                CreateLoganswer(page, false);
            }
            catch (Exception ex)
            {
                CreateFeedbackLog($"Brugeren '{page.BrugerId}'rettigheder blev ikke opdateret", page.Lbnr, ex);
                CreateLoganswer(page, true);
            }
        }

        private void RemoveUser(SPEntity.SagsLogService.Misc_Sagslog page)
        {
            try
            {
                if (!string.IsNullOrEmpty(page.BrugerId) && !string.IsNullOrEmpty(page.URLGruppe))
                {
                    string weburl = _rooturl.TrimEnd('/') + page.SPUrl;
                    using (var web = new SPEntityWeb(weburl))
                    {
                        web.RemoveUser(page.BrugerId);
                    }
                    CreateFeedbackLog($"Brugeren '{page.BrugerId}' er blevet fjernet fra sitet.", page.Lbnr, null);
                }

                CreateLoganswer(page, false);
            }
            catch (Exception ex)
            {
                CreateFeedbackLog($"Brugeren '{page.BrugerId}' blev ikke fjernet fra sitet.", page.Lbnr, ex);
                CreateLoganswer(page, true);
            }
        }

        

        #endregion
        

        #region Sagslog
        private void CreateSite(SPEntity.SagsLogService.Misc_Sagslog page)
        {
            try
            {
                //var ndata = new NavEntityData(_wsurl);
                //var foldere = ndata.HentFoldere(page.URLGruppe);
                //var brugere = ndata.HentBrugere(page.Noeglevaerdi);

                uint lang = 1030;
                if (page.LanguageId != 0)
                    lang = (uint)page.LanguageId;


                NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

                string webUrl = SPEntityWeb.CreateEntityWeb(page.Template, page.Noeglevaerdi, page.Sagsbeskrivelse, _rooturl, page.SPUrl, lang, logger);
                var web = new SPEntityWeb(webUrl);
                web.AddProperty(SPEntityWeb.Sagsnrproperty, page.Noeglevaerdi);
                web.AddProperty(SPEntityWeb.Companyproperty, _company);
                web.AddProperty(SPEntityWeb.Templateproperty, page.Template);

                /*
                try
                {
                    web.CreateFolders(page.StdDocLib, foldere);

                }
                catch (Exception ex)
                {
                    CreateFeedbackLog("Dokumentmapperne kunne ikke oprettes", page.Lbnr, ex);
                }


                foreach (var bruger in brugere)
                {
                    try
                    {
                        web.AddUser(bruger.Brugernavn, NavEntityData.NavRolleTilSpRolle(bruger.NavRolle));
                    }
                    catch (Exception ex)
                    {
                        CreateFeedbackLog($"Brugeren ({bruger.Brugernavn}) kunne ikke tilføjes til sitet", page.Lbnr, ex);
                    }
                }
                */

                web.Dispose();

                CreateFeedbackLog("Sitet blev oprettet.", page.Lbnr, null);

                CreateLoganswer(page, false);
            }
            catch (Exception ex)
            {
                CreateFeedbackLog("Sitet kunne ikke oprettes.", page.Lbnr, ex);

                CreateLoganswer(page, true);
            }
        }

        private void DeleteSite(SPEntity.SagsLogService.Misc_Sagslog page)
        {
            string webUrl = _rooturl + page.SPUrl;

            using (var web = new SPEntityWeb(webUrl))
            {
                new NavEntityData(_wsurl).HentUrlgruppe(page.URLGruppe);
                web.DeleteAndCheckForDocuments(page.StdDocLib);
            }
        }

        private void UpdateSite(SPEntity.SagsLogService.Misc_Sagslog page)
        {
            try
            {
                string webUrl = _rooturl + page.SPUrl;
                using (var web = new SPEntityWeb(webUrl))
                {
                    var keys = web.GetProperties().Where(s => s.Key == SPEntityWeb.Sagsnrproperty);
                    var keyValuePairs = keys as KeyValuePair<string, string>[] ?? keys.ToArray();
                    if (keyValuePairs.Length != 1)
                        throw new Exception("Sagssitet har ikke et entydig sagsnummer");
                    if (keyValuePairs.First().Value != page.Noeglevaerdi)
                        web.SetProperty(SPEntityWeb.Sagsnrproperty, page.Noeglevaerdi);

                    if (!String.IsNullOrEmpty(page.Sagsbeskrivelse))
                    {
                        var title = $"{page.Noeglevaerdi} {page.Sagsbeskrivelse}";
                        web.SetTitle(title);
                        web.SetDescription("");
                    }
                    else
                        web.SetTitle(page.Noeglevaerdi);                        
                }
            }
            catch (Exception ex)
            {
                CreateFeedbackLog("Sitet kunne ikke opdateres.", page.Lbnr, ex);

                CreateLoganswer(page, true);
            }
        }        

        #endregion

        #region MISC
        private void CreateLoganswer(SPEntity.SagsLogService.Misc_Sagslog page, bool iserror)
        {
            page.Fejl = iserror;
            page.Behandlet = true;
            page.BehandletSpecified = true;
            page.Behandlet_den = DateTime.Now;
            page.Behandlet_denSpecified = true;

            //logproxy.Update(ref page);
            (new NavEntityData(_wsurl)).UpdateLogItem(page);
        }

        private void CreateFeedbackLog(string msg, int lbnr, Exception ex)
        {
            (new NavEntityData(_wsurl)).CreateFeedbackLog(msg, lbnr, ex);
            /*
            string _msg = "SharePoint ændringslogbeskr: " + msg + "\nLbnr: " + lbnr.ToString();  
            if (ex != null)
                _msg += "\nEx.Message: " + ex.Message;
      
            writeToEventLog(_msg, System.Diagnostics.EventLogEntryType.Information);
            */

        }

        private static void WriteToEventLog(string message, EventLogEntryType eventtype)
        {
            const string cs = "EntityService";

            var elog = new EventLog();
            if (!EventLog.SourceExists(cs))
            {
                EventLog.CreateEventSource(cs, cs);
            }

            elog.Source = cs;
            elog.WriteEntry(message, eventtype);
            // elog.EnableRaisingEvents = true;

        }
        
        #endregion
    }


}
