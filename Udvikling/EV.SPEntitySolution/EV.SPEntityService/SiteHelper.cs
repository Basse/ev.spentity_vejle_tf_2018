﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Description;
using EV.SPEntity;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Navigation;
using Microsoft.SharePoint.Utilities;

namespace EV.SPEntityService
{
    public class SiteHelper
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        internal static string CreateSite(string wsurl, string rooturl, string company, bool createDocFolders, string urlgruppe, string sagsnr, string description, string latesttargetusr, string defaultmemeberuser, string nav2015sagskorturl)
        {
            Logger.Debug($"Opretter site: Sagsnr: {sagsnr}, urlgruppe:{urlgruppe}");

            //Logger.Debug("ndata - 1 ");
            var ndata = new NavEntityData(wsurl);
            //Logger.Debug("ndata - 2 ");
          //var foldere = ndata.HentFoldere(urlgruppe);
            //Logger.Debug("ndata - 3 ");
            //var brugere = ndata.HentBrugere(sagsnr);
            //Logger.Debug("ndata - 4 ");
            var urlgruppePage = ndata.HentUrlgruppe(urlgruppe);
            //Logger.Debug("ndata - 5 ");
            var sharepointUrl = ndata.DanSagssiteUrl(sagsnr, urlgruppe);
            //Logger.Debug("ndata - 6 ");
            sharepointUrl = CleanupUrl(sharepointUrl);
            //Logger.Debug("ndata - 7 ");

            string sitetitel = string.Empty;

            uint lang = 1030;

            if (urlgruppePage.LanguageId != 0)
                lang = (uint)urlgruppePage.LanguageId;

            try
            {
                //Logger.Debug("1. Create entityWEB");
                var webUrl = SPEntityWeb.CreateEntityWeb(urlgruppePage.Template, sagsnr, "", rooturl, sharepointUrl, lang, Logger);
                //Logger.Debug("1. Create entityWEB Done");

                if (string.IsNullOrEmpty(webUrl))
                    return string.Empty;

                var web = new SPEntityWeb(webUrl);

                //web.ClearEVProperties();
                web.AddProperty(SPEntityWeb.Sagsnrproperty, sagsnr);
                web.AddProperty(SPEntityWeb.Companyproperty, company);
                web.AddProperty(SPEntityWeb.Templateproperty, urlgruppePage.Template);

                if (!string.IsNullOrEmpty(description))
                {
                    var title = $"{sagsnr} {description}";
                    web.SetTitle(title);
                    Logger.Debug($"Ændre site title til: [{title}]");
                    sitetitel = title;
                }

                if (!string.IsNullOrEmpty(nav2015sagskorturl))
                {
                    Logger.Debug($"Ændre nav2015sagskorturl. "); 
                    ChangeNavprojektssagLink(web, nav2015sagskorturl, sagsnr);
                }
                //ChangeRekvisitionsLinks(web, company, sagsnr);
                //ChangePlanlaegningsbudgetLink(web, company, sagsnr);

                if (!string.IsNullOrEmpty(latesttargetusr))
                    ChangeLatestTargetUser(web, latesttargetusr);

                if (!string.IsNullOrEmpty(defaultmemeberuser))
                {
                    web.AddUser(defaultmemeberuser, NavEntityData.NavRolleTilSpRolle("MEMBERS"));
                    Logger.Debug($"Tilføjer Def. User [{defaultmemeberuser}] til sitet.");
                }

                /*
                try
                {
                    if (foldere.Any() && createDocFolders)
                        web.CreateFolders(urlgruppePage.DocLibrary, foldere);

                }
                catch (Exception ex)
                {
                    Logger.Error($"Error creating folders for site: Sagsnr: {sagsnr}, Url:{sharepointUrl}");
                    Logger.Error(ex.Message);
                }
                

                if (!string.IsNullOrEmpty(defaultmemeberuser))
                {
                    brugere.Add(new Bruger { Brugernavn = defaultmemeberuser, NavRolle = "MEMBERS" });
                    Logger.Debug($"Tilføjer Def. User [{defaultmemeberuser}] til sitet.");
                        web.AddUser(bruger.Brugernavn, NavEntityData.NavRolleTilSpRolle(bruger.NavRolle));
                    
                }

                foreach (var bruger in brugere)
                {
                    try
                    {
                        web.AddUser(bruger.Brugernavn, NavEntityData.NavRolleTilSpRolle(bruger.NavRolle));
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(
                            $"brugeren kunne ikke tilføjes site: Sagsnr: {sagsnr}, Url:{sharepointUrl}, User: {bruger.Brugernavn}");
                        Logger.Error(ex.Message);
                    }
                }

                */

                if (string.IsNullOrEmpty(sitetitel))
                    sitetitel = web.Title;

                web.Dispose();

            }
            catch (Exception ex)
            {
                Logger.Error($"Error creating site: Sagsnr: {sagsnr}, Url:{sharepointUrl}");
                Logger.Error(ex.Message);
                return string.Empty;
            }

            Logger.Debug($"Site oprettet: Sagsnr: {sagsnr}, url:{sharepointUrl}");
            Logger.Debug($"Titlen på sitet: {sitetitel}");
            return sharepointUrl;
        }


        public static void UpdateNavigationNodeAudience(SPWeb web, SPNavigationNode node, Guid[] spAudienceIDs, string[] spGroupNames, string[] adGroupNames)
        {
            var audienceProps = new Dictionary<string, string>
            {
                ["SharepointAudienceID"] = spAudienceIDs == null
                    ? string.Empty
                    : string.Join(",", spAudienceIDs.Select(a => a.ToString()).ToArray()),
                ["SharepointGroup"] = spGroupNames == null ? string.Empty : string.Join(",", spGroupNames),
                ["ActiveDirectoryGroupObjectLDAPPath"] = adGroupNames == null
                    ? string.Empty
                    : string.Join("\n", adGroupNames)
            };
            var audienceValue =
                $"{audienceProps["SharepointAudienceID"]};;{audienceProps["ActiveDirectoryGroupObjectLDAPPath"]};;{audienceProps["SharepointGroup"]}";
            node.Properties["Audience"] = audienceValue;
            node.Update();
        }
        private static void ChangeLatestTargetUser(SPEntityWeb web, string usrname)
        {
            var ql = web.Navigation.QuickLaunch;
            var heading = ql.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "seneste");
            if (heading == null) return;

            var spGroupNames = new[] {usrname};
            UpdateNavigationNodeAudience(web.Web, heading, null, spGroupNames, null);
            Logger.Debug($"Seneste (Header) audience er sat til [{usrname}]");
        }

        private static void ChangeNavprojektssagLink(SPEntityWeb web, string navurl, string sagsnr)
        {
            var ql = web.Navigation.QuickLaunch;
            var sb = new StringBuilder();

            SPNavigationNode spn = null;

            foreach (SPNavigationNode theading in web.Navigation.QuickLaunch)
            {
                if (theading.Title.Equals("NAV projektsagskort"))
                    spn = theading;

                foreach (SPNavigationNode child in theading.Children)
                {
                    if (child.Title.Equals("NAV projektsagskort"))
                        spn = child;
                }

                if (spn != null)
                    break;
            }

            //    Logger.Debug($"QuickLaunch entries: {sb}");

            //var heading = ql.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.Equals("NAV projektsagskort"));
            if (spn == null)
            {
                Logger.Debug($"NAV projektsagskort URL blev IKKE fundet på skabelonen.");
                return;
            }

            // dynamicsnav://nav17-app1.vejle.dk:7046/TEST_CLIENT-RTC/runpage?page=6107108&$filter='No.'%20IS%20 '05008'
            spn.Url = $"{navurl}'{sagsnr}'";
            spn.Update();
            Logger.Debug($"NAV projektsagskort URL blev opdateret [{navurl}]");
        }

        private static void ChangePlanlaegningsbudgetLink(SPEntityWeb web, string company, string sagsnr)
        {
            var ql = web.Navigation.QuickLaunch;
            var heading = ql.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "ressourceplanlægning");
            if (heading == null)
            {
                SPNavigationNode item;
                heading = ql.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "sagsoversigt");
                if (heading == null)
                {
                    heading = ql.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "værktøjer");
                    if (heading == null) return;

                    item = heading.Children.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "ressourceplanlægning");
                    if (item == null) return;
                    item.Url = $"{item.Url}?c={company}&sagsnr={sagsnr}&planlaegningsbudget=true";
                    item.Update();
                }
                else
                {
                    item = heading.Children.Cast<SPNavigationNode>()
                        .FirstOrDefault(n => n.Title.ToLower() == "ressourceplanlægning");
                    if (item == null) return;
                    item.Url = $"{item.Url}?c={company}&sagsnr={sagsnr}&planlaegningsbudget=true";
                    item.Update();
                }
                Logger.Debug($"Planlægningsbudget URL (Item) blev opdateret [{item.Url}]");
            }
            else
            {
                heading.Url = $"{heading.Url}?c={company}&sagsnr={sagsnr}&planlaegningsbudget=true";
                heading.Update();
                Logger.Debug($"Planlægningsbudget URL (Header) blev opdateret [{heading.Url}]");
            }
        }


        private static void ChangeRekvisitionsLinks(SPEntityWeb web, string company, string sagsnr)
        {
            var found = false;
            var ql = web.Navigation.QuickLaunch;
            var heading = ql.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "rekvisitioner");
            if (heading != null)
            {
                heading.Url = String.Format("{0}?c={1}&sagsnr={2}/Sagsfilter/{2}", heading.Url, company, sagsnr);
                heading.Update();
                Logger.Debug($"Rekvisition URL (Header) blev opdateret [{heading.Url}]");
                found = true;
            }

            if (found) return;
            heading = ql.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "sagsoversigt");
            SPNavigationNode item;
            if (heading != null)
            {
                // If the heading has a SharePoint Dev Center item, get it.
                item = heading.Children.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "rekvisitioner");

                if (item != null)
                {
                    item.Url = String.Format("{0}?c={1}&sagsnr={2}/Sagsfilter/{2}", item.Url, company, sagsnr);
                    item.Update();
                    Logger.Debug($"Rekvisition URL (Item) blev opdateret [{item.Url}]");
                    found = true;
                }
            }

            if (found) return;
            // If a Resources heading exists, get it.
            heading = ql.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "værktøjer");
            if (heading == null) return;
            item = heading.Children.Cast<SPNavigationNode>().FirstOrDefault(n => n.Title.ToLower() == "rekvisitioner");

            if (item == null) return;
            item.Url = String.Format("{0}?c={1}&sagsnr={2}/Sagsfilter/{2}", item.Url, company, sagsnr);
            item.Update();
            Logger.Debug($"Rekvisition URL (Item) blev opdateret [{item.Url}]");
        }

        private static string CleanupUrl(string sharepointUrl)
        {
            var pos = sharepointUrl.LastIndexOf("/", StringComparison.Ordinal);
            var sagsnrOrg = sharepointUrl.Substring(pos + 1);
            var sagsnrEncoded = System.Text.RegularExpressions.Regex.Replace(sagsnrOrg, @"\s?\.?\,?", "");
            //sagsnr_encoded = System.Text.RegularExpressions.Regex.Replace(sagsnr_encoded, @"[æøåÆØÅ]", "");
            sagsnrEncoded = System.Text.RegularExpressions.Regex.Replace(sagsnrEncoded, @"[æÆ]", "ae");
            sagsnrEncoded = System.Text.RegularExpressions.Regex.Replace(sagsnrEncoded, @"[åÅ]", "aa");
            sagsnrEncoded = System.Text.RegularExpressions.Regex.Replace(sagsnrEncoded, @"[øØ]", "oe");
            sagsnrEncoded = SPEncode.UrlEncode(sagsnrEncoded);
            return sharepointUrl.Replace(sagsnrOrg, sagsnrEncoded);
        }

        internal static void UpdateSite(string rooturl, string spurl, string sagsnr, string beskrivelse)
        {
            Logger.Debug($"Updating site: Sagsnr: {sagsnr}, Url:{spurl}");

            try
            {
                if (rooturl.EndsWith("/")) rooturl = rooturl.TrimEnd('/');
                if (!spurl.StartsWith("/")) spurl = spurl.Insert(0, "/");

                string webUrl = rooturl + spurl;
                using (var web = new SPEntityWeb(webUrl))
                {
                    var keys = web.GetProperties().Where(s => s.Key == SPEntityWeb.Sagsnrproperty);
                    var keyValuePairs = keys as KeyValuePair<string, string>[] ?? keys.ToArray();
                    if (keyValuePairs.Length != 1)
                        throw new Exception("Sagssitet har ikke et entydigt sagsnummer");
                    if (keyValuePairs.First().Value != sagsnr)
                        web.SetProperty(SPEntityWeb.Sagsnrproperty, sagsnr);

                    if (!string.IsNullOrEmpty(beskrivelse))
                    {
                        web.SetTitle($"{sagsnr} {beskrivelse}");
                        web.SetDescription("");
                    }
                    else
                        web.SetTitle(sagsnr);

                }
                Logger.Debug($"Site update completed: Sagsnr: {sagsnr}, Url:{spurl}");
            }
            catch (Exception ex)
            {
                Logger.Error($"Sagssite kunne ikke opdateres: Sagsnr: {sagsnr}, Url:{spurl}");
                Logger.Error(ex.Message);
            }
        }

        internal static void UpdateUsers(string wsurl, string rooturl, string spurl, string sagsnr)
        {
            Logger.Debug($"Opdaterer rettigheder: Sagsnr: {sagsnr}, url:{spurl}");

            try
            {
                if (rooturl.EndsWith("/")) rooturl = rooturl.TrimEnd('/');
                if (!spurl.StartsWith("/")) spurl = spurl.Insert(0, "/");

                string webUrl = rooturl + spurl;
                using (var web = new SPEntityWeb(webUrl))
                {
                    var toRemove = (from SPRoleAssignment ass in web.Web.RoleAssignments select ass.Member).ToList();

                    foreach (var p in toRemove)
                        web.Web.RoleAssignments.Remove(p);

                    Logger.Debug($"Rettigheder nulstillet: Sagsnr: {sagsnr}, url:{spurl}");

                    /*
                    var ndata = new NavEntityData(wsurl);
                    var brugere = ndata.HentBrugere(sagsnr);
                    */

                    //if (! string.IsNullOrEmpty(defaultmemberuser))
                    //    brugere.Add(new Bruger {Brugernavn = defaultmemberuser, NavRolle = "MEMBERS"});

                    /*
                    foreach (var bruger in brugere)
                    {
                        try
                        {
                            web.AddUser(bruger.Brugernavn, NavEntityData.NavRolleTilSpRolle(bruger.NavRolle));
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(
                                $"brugeren kunne ikke tilføjes site: Sagsnr: {sagsnr}, Url:{spurl}, User: {bruger.Brugernavn}");
                            Logger.Error(ex.Message);
                        }
                    }
                    */
                    Logger.Debug($"Opdaterer rettigheder: Sagsnr: {sagsnr}, url:{spurl}");
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"Fejl ved opdatering af rettigheder: Sagsnr: {sagsnr}, Url:{spurl}");
                Logger.Error(ex.Message);
            }


        }
    }
}
