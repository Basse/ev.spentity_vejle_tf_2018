﻿using System.Configuration;

namespace EV.SPEntityService
{
    public class CompanyConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("Companies")]
        public CompanyCollection CompanyItems => ((CompanyCollection)(base["Companies"]));
    }

    [ConfigurationCollection(typeof(CompanyElement))]
    public class CompanyCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CompanyElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CompanyElement)(element)).CompanyName;
        }

        public CompanyElement this[int idx] => (CompanyElement)BaseGet(idx);
    }

    public class CompanyElement : ConfigurationElement
    {

        [ConfigurationProperty("companyName", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string CompanyName
        {
            get => (string) (base["companyName"]);

            set => base["companyName"] = value;
        }

        [ConfigurationProperty("wsFunctionUrl", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string WsFunctionUrl
        {
            get => (string)(base["wsFunctionUrl"]);
            set => base["wsFunctionUrl"] = value;
        }

        [ConfigurationProperty("rootUrl", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string RootUrl
        {
            get => (string)(base["rootUrl"]);
            set => base["rootUrl"] = value;
        }

        [ConfigurationProperty("nav2015sagskorturl", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string nav2015sagskorturl
        {
            get => (string)(base["nav2015sagskorturl"]);
            set => base["nav2015sagskorturl"] = value;
        }

        [ConfigurationProperty("LatestTargetUsr", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string LatestTargetUsr
        {
            get => (string)(base["LatestTargetUsr"]);
            set => base["LatestTargetUsr"] = value;
        }

        [ConfigurationProperty("DefaultMemeberUser", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string DefaultMemeberUser
        {
            get => (string)(base["DefaultMemeberUser"]);
            set => base["DefaultMemeberUser"] = value;
        }

        [ConfigurationProperty("createdocfolders", DefaultValue = true, IsKey = false, IsRequired = false)]
        public bool CreateDocFolders
        {
            get => (bool)(base["createdocfolders"]);
            set => base["createdocfolders"] = value;
        }


    }
}
