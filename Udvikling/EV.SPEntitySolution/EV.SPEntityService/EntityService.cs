﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using Quartz;
using Quartz.Impl;
using System.Configuration;
using EV.SPEntityService.Properties;

namespace EV.SPEntityService
{
    public partial class EntityService : ServiceBase
    {
        private void writeToEventLog(string message, EventLogEntryType eventtype)
        {
            const string cs = "N2015EntityService";
            try
            {
                var elog = new EventLog();

                if (!EventLog.SourceExists(cs))
                {                    
                    EventLog.CreateEventSource(cs, cs);
                }

                elog.Source = cs;
                elog.
                WriteEntry(message, eventtype);
                // elog.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public EntityService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //Thread.Sleep(new TimeSpan(0, 0, 20)); //allow time to attack debugger!

            //companies
            var section = (CompanyConfigurationSection)ConfigurationManager.GetSection("NavCompanies");

            ISchedulerFactory schedFact = new StdSchedulerFactory();
            // get a scheduler
            IScheduler sched = schedFact.GetScheduler();

            int count = 0;

            //Iterate companies            
            foreach (CompanyElement company in section.CompanyItems)
            {
                try
                {
                    // construct job info
                    //JobDetail saglogJob = new JobDetail(company.CompanyName, null, typeof(SagsLogJob));
                    var saglogJob = new JobDetail(company.CompanyName, null, typeof(SagsOpdateringJob));
                    saglogJob.JobDataMap.Add("wsurl", company.WsFunctionUrl);
                    saglogJob.JobDataMap.Add("rooturl", company.RootUrl);
                    saglogJob.JobDataMap.Add("createdocfolders", company.CreateDocFolders);
                    saglogJob.JobDataMap.Add("company", company.CompanyName);
                    saglogJob.JobDataMap.Add("latesttargetusr", company.LatestTargetUsr);
                    saglogJob.JobDataMap.Add("defaultmemeberuser", company.DefaultMemeberUser);
                    saglogJob.JobDataMap.Add("nav2015sagskorturl", company.nav2015sagskorturl);

                    // fire every x second hour
                    Trigger trigger = TriggerUtils.MakeSecondlyTrigger(Settings.Default.Interval);
                    // start on the next even hour

                    trigger.StartTimeUtc = DateTime.UtcNow.AddSeconds(count * 15);
                    trigger.Name = company.CompanyName + "_Trigger";
                    sched.ScheduleJob(saglogJob, trigger);

                    count++;
                    writeToEventLog("Job Scheduled for CompanyName: " + company.CompanyName + " - WSFunctionUrl: " + company.WsFunctionUrl + " - RootUrl:"+company.WsFunctionUrl + " - nav2015sagskorturl: " + company.nav2015sagskorturl, EventLogEntryType.Information);
                }
                catch (Exception ex)
                {
                    writeToEventLog("Job Scheduled failed: " + ex.Message, EventLogEntryType.Error);
                }
            }

            var mailJob = new JobDetail("MailJob", null, typeof(StatusMailJob));
            var mTrigger = TriggerUtils.MakeDailyTrigger("MailTrigger", 1, 0);
            mTrigger.StartTimeUtc = DateTime.UtcNow;
            sched.ScheduleJob(mailJob, mTrigger);

            sched.Start();
        }

        protected override void OnStop()
        {
            writeToEventLog("Service stopped", EventLogEntryType.Information);

        }
    }
}
