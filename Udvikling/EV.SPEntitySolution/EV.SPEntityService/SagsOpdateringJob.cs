﻿using System;
using System.Globalization;
using System.Linq;
using Quartz;
using EV.SPEntityService.SagsaendringsService;

namespace EV.SPEntityService
{
    public class SagsOpdateringJob : IStatefulJob
    {
        private string _rooturl;
        private string _wsurl;
        private string _company;
        private bool _createSubFolders;
        private string _latesttargetusr;
        private string _defaultmemeberuser;
        private string _nav2015sagskorturl;

        //private bool Debug;


        Misc_Sagsaendringslog_Service _logproxy;

        private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public void Execute(JobExecutionContext context)
        {
            _wsurl = context.JobDetail.JobDataMap.Get("wsurl") as string;
            _rooturl = context.JobDetail.JobDataMap.Get("rooturl") as string;
            _company = context.JobDetail.JobDataMap.Get("company") as string;
            _latesttargetusr = context.JobDetail.JobDataMap.Get("latesttargetusr") as string;
            _defaultmemeberuser = context.JobDetail.JobDataMap.Get("defaultmemeberuser") as string;
            _createSubFolders = true;
            var s = context.JobDetail.JobDataMap.Get("createdocfolders") as string;
            if (s != null && (context.JobDetail.JobDataMap.Get("createdocfolders") != null && s.Trim().ToLower().Equals("false")))
                _createSubFolders = false;

            _nav2015sagskorturl = context.JobDetail.JobDataMap.Get("nav2015sagskorturl") as string;
            ProcessLog();
        }

        public void DebugExecute(string wsurl, string rooturl, string company, bool createsubfolders, string nav2015sagskorturl, string latesttargetusr, string defaultmemeberuser)
        {
            _wsurl = wsurl;
            _rooturl = rooturl;
            _company = company;
            _createSubFolders = createsubfolders;
            _nav2015sagskorturl = nav2015sagskorturl;
            _latesttargetusr = latesttargetusr;
            _defaultmemeberuser = defaultmemeberuser;

            ProcessLog();
        }

        private enum UpdateMethods
        {
            Create,
            UpdateSite,
            UpdateUsers,
            UpdateSiteAndUsers,
            Ignore
        }

        private void ProcessLog()
        {
            try
            {
                _logproxy = new Misc_Sagsaendringslog_Service
                               {
                                   UseDefaultCredentials = true,
                                   Url = _wsurl + "/Page/Misc_Sagsaendringslog"
                               };
                var pages1 = _logproxy.ReadMultiple(new[] { new Misc_Sagsaendringslog_Filter { Field = Misc_Sagsaendringslog_Fields.Sag_rettet, Criteria = "1" } }, null, 0);
                var pages2 = _logproxy.ReadMultiple(new[] { new Misc_Sagsaendringslog_Filter { Field = Misc_Sagsaendringslog_Fields.Sagsadgang_rettet, Criteria = "1" } }, null, 0);
                var allpages = pages1.Concat(pages2);
                var pages = from a in allpages
                            group a by a.No into grouped
                            select grouped.First();

                //var pages = allpages.Where(p => p.Sag_rettet || p.Sagsadgang_rettet);

                var enumerable = pages as Misc_Sagsaendringslog[] ?? pages.ToArray();
                if (enumerable.Any())
                    _logger.Debug($"{enumerable.Length} records til behandling");

                //foreach (var page in pages.Where(p=>p.Sag_rettet || p.Sagsadgang_rettet))
                for (var i = 0; i < enumerable.Length; i++)
                {
                    _logger.Debug("Behandler index: " + i.ToString(CultureInfo.InvariantCulture));

                    var page = enumerable.ElementAt(i);

                    var updateMethod = GetUpdateMethod(page);

                    switch (updateMethod)
                    {
                        case UpdateMethods.Create:
                            CreateSite(ref page);
                            break;

                        case UpdateMethods.UpdateSite:
                            UpdateSite(ref page);
                            break;
                        /*
                                                case UpdateMethods.UpdateUsers:
                                                    UpdateUsers(ref page);
                                                    break;
                                                case UpdateMethods.UpdateSiteAndUsers:
                                                    UpdateSite(ref page);
                                                    UpdateUsers(ref page);
                                                    break;
                                                case UpdateMethods.Ignore:
                                                    break;
                                                    */
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Fejl ved hentning af oversigt: "+ ex.Message);
            }
        }

        private void CreateSite(ref Misc_Sagsaendringslog page)
        {
            try
            {
                var url = SiteHelper.CreateSite(_wsurl, _rooturl, _company, _createSubFolders, page.SharePoint_URL_gruppe, page.No, page.Description, _latesttargetusr, _defaultmemeberuser, _nav2015sagskorturl);

                var temppage = _logproxy.Read(page.No);
                temppage.SharePoint_URL_adresse = url;
                temppage.Sag_rettet = false;
                temppage.Sag_rettetSpecified = true;

                temppage.Sagsadgang_rettet = false;
                temppage.Sagsadgang_rettetSpecified = true;

                _logproxy.Update(ref temppage);
            }
            catch (Exception ex)
            {
                _logger.Error(
                    $"Oprettelse af sagssite fejlede: Sagsnr: {page.No}, UrlGruppe:{page.SharePoint_URL_gruppe}");
                _logger.Error(ex.Message);
            }
        }

/*
        /// <summary>
        /// Rekursiv opdateing af brugerrettigheder
        /// Hvis Sag_rettet_den er blevet opdateret i mellem opdateringerne finder sted skal funktionen
        /// køre sig selv igen
        /// </summary>
        /// <param name="page"></param>
        private void UpdateUsers(ref Misc_Sagsaendringslog page)
        {
            try
            {
                DateTime firstTime = page.Sagsadgang_den;

                SiteHelper.UpdateUsers(_wsurl, _rooturl, page.SharePoint_URL_adresse, page.No);

                page.Sagsadgang_rettet = false;
                page.Sagsadgang_rettetSpecified = true;

                _logproxy.Update(ref page);

                if (firstTime != page.Sagsadgang_den)
                {
                    _logger.Debug("Sagsadgang er forældet. Opdatere igen. Firsttime: " + firstTime.ToString(CultureInfo.InvariantCulture) + " New: " + page.Sagsadgang_den.ToString(CultureInfo.InvariantCulture));
                    UpdateUsers(ref page);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(
                    $"Opdatering af brugerliste fejlede: Sagsnr: {page.No}, Url:{page.SharePoint_URL_adresse}");
                _logger.Error(ex.Message);
            }
        }
*/



        /// <summary>
        /// Rekursiv opdateing af sagssite
        /// Hvis Sag_rettet_den er blevet opdateret i mellem opdateringerne finder sted skal funktionen
        /// køre sig selv igen
        /// </summary>
        /// <param name="page"></param>
        private void UpdateSite(ref Misc_Sagsaendringslog page)
        {
            try
            {
                var firstTime = page.Sag_rettet_den;

                SiteHelper.UpdateSite(_rooturl, page.SharePoint_URL_adresse, page.No, page.Description);

                var temppage = _logproxy.Read(page.No);

                temppage.Sag_rettet = false;
                temppage.Sag_rettetSpecified = true;

                _logproxy.Update(ref temppage);

                if (firstTime != page.Sag_rettet_den)
                    UpdateSite(ref temppage);
            }
            catch (Exception ex)
            {
                _logger.Error($"Opdatering af sagssite fejlede: Sagsnr: {page.No}, Url:{page.SharePoint_URL_adresse}");
                _logger.Error(ex.Message);
            }
        }


        private static UpdateMethods GetUpdateMethod(Misc_Sagsaendringslog page)
        {
            if (page.Sag_rettet && !string.IsNullOrEmpty(page.SharePoint_URL_gruppe) && string.IsNullOrEmpty(page.SharePoint_URL_adresse))
            {
                return UpdateMethods.Create;
            }
            if (page.Sag_rettet && !string.IsNullOrEmpty(page.SharePoint_URL_adresse) && !page.Sagsadgang_rettet)
            {
                return UpdateMethods.UpdateSite;
            }
            if (page.Sag_rettet && !string.IsNullOrEmpty(page.SharePoint_URL_adresse) && !page.Sagsadgang_rettet)
            {
                return UpdateMethods.UpdateSiteAndUsers;
            }
            return page.Sagsadgang_rettet ? UpdateMethods.UpdateUsers : UpdateMethods.Ignore;
        }

    }
}
