﻿using System;
using Quartz;
using EV.SPEntityService.Properties;
using System.IO;
using System.Reflection;

namespace EV.SPEntityService
{
    public class StatusMailJob : IStatefulJob
    {     
        public void Execute(JobExecutionContext context)
        {
            if (!Settings.Default.SendStatusMail) return;
            var msg = new EMailMessage("NHK Sagssiteservice");
            msg.AddRecipient(Settings.Default.StatusMailReciever);
            msg.Subject = "NHK Sagssiteservice log - " + DateTime.Now.AddDays(-1).ToShortDateString();

            var dirname = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var dt = DateTime.Now.AddDays(-1);
                
            var logfilename = $@"\Log_{dt:yyyy}-{dt:MM}-{dt:dd}.csv";
            var logname = dirname + @"\" + logfilename;  //string.Format(@"\Log_{0}-{1}-{2}.csv", dt.ToString("yyyy"), dt.ToString("MM"), dt.ToString("dd"));

            if (!File.Exists(logname)) return;
            using (var fs = File.OpenRead(logname))
            {                        
                msg.AddAttachment(fs, logfilename);
                msg.SendMessage();
            }
        }
    }
}
