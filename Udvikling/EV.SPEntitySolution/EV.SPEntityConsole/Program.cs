﻿using System.Linq;
using System.Xml;
using EV.SPEntity;
using System;
using EV.SPEntityService;


namespace EV.SPEntityConsole
{

    public class Conpanyinfo
    {
        public string CompanyName;
        public string WsFunctionurl;
        public string RootUrl;
        public string nav2015sagskorturl;
        public string latesttargetusr;
        public string defaultmemeberuser;
    }

    internal class Program
    {
        private static string Soegconfig(string soegestr, string src)
        {
            var urlstr = src;
            var startidx = urlstr.IndexOf(soegestr, StringComparison.Ordinal);
            if (startidx <= 0) return string.Empty;
            startidx += soegestr.Length + 1;
            urlstr = urlstr.Substring(startidx);
            urlstr = urlstr.Remove(urlstr.IndexOf('\"'));
            urlstr = urlstr.Trim();
            return urlstr;
        }

        private static void Main()
        {
            //SPSecurity.RunWithElevatedPrivileges(() =>
            //{
            //    using (var site = new SPSite("http://evudv-sp2013/"))
            //    {
            //        using (var web = site.OpenWeb())
            //        {
            //            // Check site for Rekvisition quick link.
            //            // Get the Quick Launch headings.
            //            var ql = web.Navigation.QuickLaunch;

            //            // If a Resources heading exists, get it.
            //            var heading = ql
            //                .Cast<SPNavigationNode>()
            //                .FirstOrDefault(n => n.Title.ToLower() == "sagsoversigt");

            //            // ss_nogf/_layouts/ev/rekv/rekvisition.aspx?c=VEJDFT&sagsnr=JNMU000#/Sagsfilter/JNMU000
            //            // If the Resources heading does not exist change it..
            //            if (heading == null) return;
            //            {
            //                // If the heading has a SharePoint Dev Center item, get it.
            //                var item = heading
            //                    .Children
            //                    .Cast<SPNavigationNode>()
            //                    .FirstOrDefault(n => n.Title.ToLower() == "rekvisitioner");

            //                if (item == null) return;
            //                item.Url = string.Format("{0}?c={1}&sagsnr={2}/Sagsfilter/{2}", heading.Url, "VEJDFT", "SAG01");
            //                item.Update();
            //            }
            //            // stuff goes here elevated
            //        }
            //    }
            //});


            try
            {

                var cfgfil = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase) +
                             "\\EV.SPEntityService.exe.config";

                var xmldoc = new XmlDocument();
                xmldoc.Load(cfgfil);

                var companies = xmldoc.GetElementsByTagName("Companies");
                var strarr = companies[0].InnerXml.Split('>');

                var listCompanyInfo = (strarr
                    .Select(urlstr => new { urlstr, tmpCompanyName = Soegconfig("companyName=", urlstr) })
                    .Where(t => !string.IsNullOrEmpty(t.tmpCompanyName))
                    .Select(t => new Conpanyinfo
                    {
                        CompanyName = t.tmpCompanyName,
                        WsFunctionurl = Soegconfig("wsFunctionUrl=", t.urlstr),
                        RootUrl = Soegconfig("rootUrl=", t.urlstr),
                        nav2015sagskorturl = Soegconfig("nav2015sagskorturl=", t.urlstr),
                        defaultmemeberuser = Soegconfig("DefaultMemeberUser=", t.urlstr),
                        latesttargetusr = Soegconfig("LatestTargetUsr=", t.urlstr)
                    })).ToList();


                Console.WriteLine(@"Læser fra: " + cfgfil);
                Console.WriteLine(@"Antal Company Info i CFG fil: " + listCompanyInfo.Count);
                if (! listCompanyInfo.Any())
                {
                    Console.WriteLine(@"Applikaiton afsluttet! Ingen company info fundet.");
                    return;
                }

                Console.WriteLine(@"Checker companies..");
                foreach (var compinfo in listCompanyInfo)
                {
                    Console.WriteLine($@"Company: {compinfo.CompanyName}");
                    Console.WriteLine($@"WSUrl: {compinfo.WsFunctionurl}");
                    Console.WriteLine($@"RootUrl: {compinfo.RootUrl}");
                    Console.WriteLine($@"nav2015sagskorturl: {compinfo.nav2015sagskorturl}");
                    Console.WriteLine($@"LatestTargetUsr: {compinfo.latesttargetusr }");
                    Console.WriteLine($@"DefaultMemeberUser: {compinfo.defaultmemeberuser}");

                    var tmpWeb = new EV.SPEntityService.SagsOpdateringJob();
                    tmpWeb.DebugExecute(compinfo.WsFunctionurl, compinfo.RootUrl, compinfo.CompanyName, false, compinfo.nav2015sagskorturl, compinfo.latesttargetusr, compinfo.defaultmemeberuser );
                    //Console.WriteLine($@"HelloWorld returned: {ndata.TestHelloWord()}");

                    /*
                    var logproxy = new Misc_Sagsaendringslog_Service
                    {
                        UseDefaultCredentials = true,
                        Url = compinfo.WsFunctionurl + "/Page/Misc_Sagsaendringslog"
                    };

                    var pages1 =
                        logproxy.ReadMultiple(
                            new[]
                                {
                                new Misc_Sagsaendringslog_Filter
                                    {
                                        Field = Misc_Sagsaendringslog_Fields.Sag_rettet,
                                        Criteria = "1"
                                    }
                                }, null, 0);
                    var pages2 =
                        logproxy.ReadMultiple(
                            new[]
                                {
                                new Misc_Sagsaendringslog_Filter
                                    {
                                        Field =
                                            Misc_Sagsaendringslog_Fields
                                            .Sagsadgang_rettet,
                                        Criteria = "1"
                                    }
                                }, null, 0);
                    var allpages = pages1.Concat(pages2);
                    var pages = from a in allpages
                                group a by a.No
                                into grouped
                                select grouped.First();

                    var enumerable = pages as Misc_Sagsaendringslog[] ?? pages.ToArray();
                    Console.WriteLine(enumerable.Any()
                        ? $@"{enumerable.Length} records til behandling"
                        : @"Applikaiton afsluttet! Ingen company info fundet.");


                    //foreach (var page in pages.Where(p=>p.Sag_rettet || p.Sagsadgang_rettet))
                    for (int i = 0; i < enumerable.Length; i++)
                    {
                        Console.WriteLine(@"Behandler index: " + i.ToString(CultureInfo.InvariantCulture));
                        var spage = enumerable.ElementAt(i);

                        var ndata = new NavEntityData(compinfo.WsFunctionurl);
                        //var foldere = ndata.HentFoldere(spage.SharePoint_URL_gruppe);
                        //var brugere = ndata.HentBrugere(spage.No);
                        var urlgruppePage = ndata.HentUrlgruppe(spage.SharePoint_URL_gruppe);
                        string sharepointUrl = ndata.DanSagssiteUrl(spage.No, spage.SharePoint_URL_gruppe);
                        sharepointUrl = CleanupUrl(sharepointUrl);

                        uint languageid = 1030;

                        if (urlgruppePage.LanguageId != 0)
                            languageid = (uint)urlgruppePage.LanguageId;

                        string mrooturl = compinfo.RootUrl;
                        string spurl = sharepointUrl;

                        // ReSharper disable ReturnValueOfPureMethodIsNotUsed
                        if (mrooturl.EndsWith("/")) mrooturl.TrimEnd('/');
                        // ReSharper restore ReturnValueOfPureMethodIsNotUsed
                        // ReSharper disable ReturnValueOfPureMethodIsNotUsed
                        if (!spurl.StartsWith("/")) spurl.Insert(0, "/");
                        // ReSharper restore ReturnValueOfPureMethodIsNotUsed

                        //string mwebUrl = mrooturl + spurl;

                        if (!compinfo.RootUrl.EndsWith("/"))
                            compinfo.RootUrl = string.Concat(compinfo.RootUrl, "/");

                        if (sharepointUrl.StartsWith("/"))
                            sharepointUrl = sharepointUrl.TrimStart('/');

                        string completeUrl = compinfo.RootUrl + sharepointUrl;

                        using (var site = new SPSite(completeUrl))
                        {

                            Console.WriteLine(@"Templates i " + completeUrl);

                            //Tjek site templates først
                            var templates = site.GetWebTemplates(languageid).OfType<SPWebTemplate>();
                            var tpTmpPlateses = templates as SPWebTemplate[] ?? templates.ToArray();

                            var stdMatches = tpTmpPlateses.Where(s => s.Title.ToUpper() == urlgruppePage.Template.ToUpper());
                            var spWebTemplates = stdMatches as SPWebTemplate[] ?? stdMatches.ToArray();

                            Console.WriteLine(@"GetWebTemplates fundet: " + spWebTemplates.Length + @" ud af " + tpTmpPlateses.Length);
                            if (spWebTemplates.Any())
                            {
                                Console.WriteLine(@"Applikaiton afsluttet! Ingen company info fundet.");
                                foreach (var tt in spWebTemplates)
                                {
                                    Console.WriteLine(@"TemplatenID: " + tt.ID);
                                    Console.WriteLine(@"TemplateTitle: " + tt.Title);
                                    Console.WriteLine(@"Templatenavn: " + tt.Name);
                                }
                                Console.WriteLine(@"Applikaiton afsluttet! Ingen company info fundet.");
                            }

                            foreach (var tpTmpPlates in tpTmpPlateses)
                            {
                                Console.WriteLine(@"TemplatenID: " + tpTmpPlates.ID);
                                Console.WriteLine(@"TemplateTitle: " + tpTmpPlates.Title);
                                Console.WriteLine(@"Templatenavn: " + tpTmpPlates.Name);
                            }


                            //Tjek Custom Templates
                            var customTemplates = site.GetCustomWebTemplates(languageid).OfType<SPWebTemplate>();
                            var webTemplates = customTemplates as SPWebTemplate[] ?? customTemplates.ToArray();
                            var customMatches = webTemplates.Where(s => s.Title.ToUpper() == urlgruppePage.Template.ToUpper());

                            var spWebTemplates1 = customMatches as SPWebTemplate[] ?? customMatches.ToArray();
                            var matches = customMatches as SPWebTemplate[] ?? spWebTemplates1.ToArray();
                            Console.WriteLine(@"GetCustomTemplates fundet: " + matches.Length + @" ud af " + webTemplates.Count());

                            if (spWebTemplates1.Any())
                            {
                                Console.WriteLine(@"Applikaiton afsluttet! Ingen company info fundet.");
                                foreach (var tt in spWebTemplates1)
                                {
                                    Console.WriteLine($@"TemplatenID: {tt.ID}");
                                    Console.WriteLine($@"TemplateTitle: {tt.Title}");
                                    Console.WriteLine($@"Templatenavn: {tt.Name}");
                                }
                                Console.WriteLine(@"Applikaiton afsluttet! Ingen company info fundet.");
                            }

                            foreach (var customMatche in matches)
                            {
                                Console.WriteLine(@"TemplatenID: " + customMatche.ID);
                                Console.WriteLine(@"TemplateTitle: " + customMatche.Title);
                                Console.WriteLine(@"Templatenavn: " + customMatche.Name);
                            }

                        }
                        Console.WriteLine(@"Applikaiton afsluttet! Ingen company info fundet.");
                    }
                    */
                }
                Console.WriteLine(@"Done.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(@"Consle Exception: " + ex.Message);
            }

        }
    }
}

/* 
             * //var web = SPEntityWeb.CreateEntityWeb("STS#0", "Sag01", "Beskrivelse til Sag01", "http://evudv-devpc-sen", "/sager/s01", 1033);

            //SagsLogJob tmpWeb = new SagsLogJob();
            //tmpWeb.DebugExecute();

//            var web = new SPEntityWeb("http://evudv-devpc-sen/sager/1324/level1-1/level2/sag1223/");

            //web.CreateFolders("Shared Documents", new string[] {"Mappe1", "Mappe2" });

            //web.AddUser("evudv/mbj", SPRoleType.Contributor);

            //web.RemoveUser("evudv/mbj");

            //web.AddProperty("ev_entitysite", "true");


//            var props = web.GetProperties();
            SPWeb web = new SPSite("http://evudv-devpc-sen/sites/WebPartManager/c/").OpenWeb();
            web.AllowUnsafeUpdates = true;
            //web.EnsureUser(loginName);
            //SPRoleAssignment assignment;

            //assignment = new SPRoleAssignment(loginName, string.Empty, string.Empty, string.Empty);
            //assignment.RoleDefinitionBindings.Add(web.RoleDefinitions.GetByType(convertRoleType(rolletype)));
            //web.RoleAssignments.Add(assignment);
            List<SPPrincipal> toRemove = new List<SPPrincipal>();
            foreach (SPRoleAssignment ass in web.RoleAssignments)
            {
                SPPrincipal p =  ass.Member as SPPrincipal;
                toRemove.Add(p);
            }

            foreach (var p in toRemove)
                web.RoleAssignments.Remove(p);
        //web.RoleAssignments
        //web.Update();
            web.AllowUnsafeUpdates = false;
             */
