﻿using System;
using System.Collections.Generic;
using System.Linq;
//using EV.SPEntity.FolderService;
//using EV.SPEntity.SagAdgangService;
//using EV.SPEntity.FeedbackLogService;
//using EV.SPEntity.SagsLogService;

namespace EV.SPEntity
{
    public class NavEntityData
    {
        public string WsUrl { get; set; }

        public NavEntityData(string wsurl)
        {
            WsUrl = wsurl;
        }

        public string TestHelloWord()
        {
            var proxy = new SharePointFunctions.SharePointFunctions
            {
                UseDefaultCredentials = true,
                Url = GetProxyUrl(WsTypes.SharePointfunktioner)
            };

            return proxy.HelloWorld();
        }

        /*
        public string[] HentFoldere(string urlgruppe)
        {
            var proxy = new Misc_URLgruppeFoldere_Service
                            {
                                UseDefaultCredentials = true,
                                Url = GetProxyUrl(WsTypes.UrlGruppeFoldere)
                            };

            var filters = new List<Misc_URLgruppeFoldere_Filter>
                              {
                                  new Misc_URLgruppeFoldere_Filter
                                      {
                                          Field = Misc_URLgruppeFoldere_Fields.URL_gruppe,
                                          Criteria = urlgruppe
                                      }
                              };

            var pages = proxy.ReadMultiple(filters.ToArray(), null, 0);

            var result = from p in pages
                         select p.Mappesti;

            return result.ToArray();
        }

        public List<Bruger> HentBrugere(string sagsnr)
        {
            var proxy = new Misc_SagAdgang_Service {UseDefaultCredentials = true, Url = GetProxyUrl(WsTypes.Sagsadgang)};

            var filters = new List<Misc_SagAdgang_Filter>
                              {
                                  new Misc_SagAdgang_Filter
                                      {
                                          Field = Misc_SagAdgang_Fields.Sagsnr,
                                          Criteria = sagsnr
                                      }
                              };

            var pages = proxy.ReadMultiple(filters.ToArray(), null, 0);

            return (from p in pages
                    select new Bruger
                    {
                        Brugernavn = p.BrugerID,
                        NavRolle = p.SharePoint_rolle.ToString()
                    }).ToList();
        }
        

        public void CreateFeedbackLog(string msg, int lbnr, Exception ex)
        {
            var logitems = new List<Misc_LogBeskrivelse>
                               {
                                   new Misc_LogBeskrivelse
                                       {
                                           Lbnr = lbnr,
                                           LbnrSpecified = true,
                                           Beskrivelse = msg.Length > 250 ? msg.Substring(0, 249) : msg
                                       }
                               };

            if (ex != null)
            {
                logitems.Add(new Misc_LogBeskrivelse
                                 {
                    Lbnr = lbnr,
                    LbnrSpecified = true,
                    Beskrivelse = ex.Message.Length > 250 ? ex.Message.Substring(0, 249) : ex.Message
                });
            }

            var proxy = new Misc_LogBeskrivelse_Service
                            {
                                Url = GetProxyUrl(WsTypes.FeedbackLog),
                                UseDefaultCredentials = true
                            };

            var logitemsarr = logitems.ToArray();
            proxy.CreateMultiple(ref logitemsarr);
        }
        */
        /*
        public Misc_Sagslog[] GetLogitems(int itemcount)
        {
            var logproxy = new Misc_Sagslog_Service {UseDefaultCredentials = true, Url = GetProxyUrl(WsTypes.Sagslog)};

            var pages = logproxy.ReadMultiple(null, null, itemcount);
            return pages;
        }

        public SagsaendringsService.Misc_Sagsaendringslog[] GetJobList(int itemcount)
        {
            var logproxy = new SagsaendringsService.Misc_Sagsaendringslog_Service
                               {
                                   UseDefaultCredentials = true,
                                   Url = GetProxyUrl(WsTypes.Sagslog)
                               };
            return logproxy.ReadMultiple(null, null, itemcount);
        }

        public void UpdateLogItem(Misc_Sagslog page)
        {
            var logproxy = new Misc_Sagslog_Service {UseDefaultCredentials = true, Url = GetProxyUrl(WsTypes.Sagslog)};

            logproxy.Update(ref page);
        }

        public List<Urlgruppe> HentUrlgrupper()
        {
            var proxy = new UrlGruppeService.Misc_URLgrupper_Service
                            {
                                UseDefaultCredentials = true,
                                Url = GetProxyUrl(WsTypes.Urlgrupper)
                            };

            var pages = proxy.ReadMultiple(null, null, 0);
            return (from p in pages
                    select new Urlgruppe
                    {
                        Name = p.URL_gruppe,
                        Description = p.Beskrivelse,
                        DocLibrary = p.Dokumentbibliotek,
                        Template = p.Template,
                        LanguageId = p.LanguageId
                    }).ToList();
        }
        */

        public string DanSagssiteUrl(string sagsnr, string urlgruppe)
        {
            var proxy = new SharePointFunctions.SharePointFunctions
                            {
                                UseDefaultCredentials = true,
                                Url = GetProxyUrl(WsTypes.SharePointfunktioner)
                            };

            return proxy.DanUrlGruppe(sagsnr, urlgruppe);
        }

        public Urlgruppe HentUrlgruppe(string urlgruppe)
        {
            var proxy = new UrlGruppeService.Misc_URLgrupper_Service
                            {
                                UseDefaultCredentials = true,
                                Url = GetProxyUrl(WsTypes.Urlgrupper)
                            };
            var page = proxy.Read(urlgruppe);

            return new Urlgruppe
                       {
                Description = page.Beskrivelse,
                // Fjernes nav 2015..
                // DocLibrary = page.Dokumentbibliotek,
                LanguageId = page.LanguageId,
                Name = page.URL_gruppe,
                Template = page.Template
            };
        }

        public string GetProxyUrl(WsTypes type)
        {
            switch (type)
            {
                //case WsTypes.Sagslog:
                //    return WsUrl + "/Page/Misc_Sagslog";
                //case WsTypes.FeedbackLog:
                //    return WsUrl + "/Page/Misc_LogBeskrivelse";
                //case WsTypes.UrlGruppeFoldere:
                //    return WsUrl + "/Page/Misc_URLgruppeFoldere";
                //case WsTypes.Sagsadgang:
                //    return WsUrl + "/Page/Misc_SagAdgang";
                case WsTypes.Urlgrupper:
                    return WsUrl + "/Page/Misc_URLgrupper";
                case WsTypes.Sagsaendringslog:
                    return WsUrl + "/Page/Misc_Sagsaendringslog";
                //case WsTypes.Sagsfunktioner:
                //    return WsUrl + "/Codeunit/Sag_Funktioner";
                case WsTypes.SharePointfunktioner:
                    return WsUrl + "/Codeunit/SharePointFunctions";
            }
            return null;
        }

        public static EvspRoleType NavRolleTilSpRolle(string strRolle)
        {
            strRolle = strRolle.ToUpper();
            //VISITOR,MEMBERS,OWNER
            switch (strRolle)
            {
                case "OWNER":
                    return EvspRoleType.Admin;
                case "MEMBERS":
                    return EvspRoleType.Member;
                case "VISITOR":
                    return EvspRoleType.Guest;
            }
            return EvspRoleType.Guest;

        }

    }

    public enum WsTypes
    {
        //Sagslog,
        //FeedbackLog,
        //UrlGruppeFoldere,
        //Sagsadgang,
        Urlgrupper,
        Sagsaendringslog,
        //Sagsfunktioner,
        SharePointfunktioner
    }   
}
