﻿namespace EV.SPEntity
{
    public class Urlgruppe
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Template { get; set; }
        //public string DocLibrary { get; set; }
        public int LanguageId { get; set; }
    }
}
