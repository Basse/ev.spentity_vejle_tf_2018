﻿using System;
using System.Linq;
using Microsoft.SharePoint.Utilities;
using System.Web;

namespace EV.SPEntity
{
    
    public class SiteHelper
    {
        //internal static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public static string GetBaseUrl()
        {
            var u = HttpContext.Current.Request.Url;

            string realurl = u.IsDefaultPort ? string.Format("{0}://{1}", u.Scheme, u.Host) : string.Format("{0}://{1}:{2}", u.Scheme, u.Host, u.Port);
            return realurl;
        }

        /*
        public static string CreateSite(string wsurl, string rooturl, string company, string urlgruppe, string sagsnr, string description)
        {
            //_logger.Debug(string.Format("Opretter site: Sagsnr: {0}, urlgruppe:{1}", sagsnr, urlgruppe));

            var ndata = new NAVEntityData(wsurl);
            var foldere = ndata.HentFoldere(urlgruppe);
            var brugere = ndata.HentBrugere(sagsnr);
            var urlgruppe_page = ndata.HentUrlgruppe(urlgruppe);
            string sharepointUrl = ndata.DanSagssiteUrl(sagsnr, urlgruppe);
            sharepointUrl = cleanupUrl(sharepointUrl);

            uint lang = 1030;

            if (urlgruppe_page.LanguageId != 0)
                lang = (uint)urlgruppe_page.LanguageId;

            string webUrl = SPEntityWeb.CreateEntityWeb(urlgruppe_page.Template, sagsnr, description, rooturl, sharepointUrl, lang);
            var web = new SPEntityWeb(webUrl);

            web.AddProperty(SPEntityWeb.SAGSNRPROPERTY, sagsnr);
            web.AddProperty(SPEntityWeb.COMPANYPROPERTY, company);
            web.AddProperty(SPEntityWeb.TEMPLATEPROPERTY, urlgruppe_page.Template);

            try
            {
                if (foldere.Any())
                    web.CreateFolders(urlgruppe_page.DocLibrary, foldere);

            }

            catch
            {
                //_logger.Error(string.Format("Error creating folders for site: Sagsnr: {0}, Url:{1}", sagsnr, sharepointUrl));
                //_logger.Error(ex.Message);
            }


            foreach (var bruger in brugere)
            {
                try
                {
                    web.AddUser(bruger.Brugernavn, NAVEntityData.NavRolleTilSPRolle(bruger.NavRolle));
                }
                catch
                {
                    //_logger.Error(string.Format("brugeren kunne ikke tilføjes site: Sagsnr: {0}, Url:{1}, User: {2}", sagsnr, sharepointUrl, bruger.Brugernavn));
                    //_logger.Error(ex.Message);
                }
            }


            web.Dispose();

            //_logger.Debug(string.Format("Site oprettet: Sagsnr: {0}, url:{1}", sagsnr, sharepointUrl));

            return sharepointUrl;
        }
        */

        private static string cleanupUrl(string sharepointUrl)
        {
            int pos = sharepointUrl.LastIndexOf("/", StringComparison.Ordinal);
            string sagsnr_org = sharepointUrl.Substring(pos + 1);
            string sagsnr_encoded = System.Text.RegularExpressions.Regex.Replace(sagsnr_org, @"\s?\.?\,?", "");
            sagsnr_encoded = System.Text.RegularExpressions.Regex.Replace(sagsnr_encoded, @"[æÆ]", "ae");
            sagsnr_encoded = System.Text.RegularExpressions.Regex.Replace(sagsnr_encoded, @"[åÅ]", "aa");
            sagsnr_encoded = System.Text.RegularExpressions.Regex.Replace(sagsnr_encoded, @"[øØ]", "oe");
            sagsnr_encoded = SPEncode.UrlEncode(sagsnr_encoded);
            return sharepointUrl.Replace(sagsnr_org, sagsnr_encoded);
        }
    }
}
