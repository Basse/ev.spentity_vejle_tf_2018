﻿using System;

namespace EV.SPEntity
{
    public class DocListItem
    {       
        public ItemTypes ItemType;
        public string Url;
        public Guid Id;
        public string Name;
        public string IconName;

        public enum ItemTypes
        {
            File,
            Folder
        }
    }
}
