﻿namespace EV.SPEntity
{
    public class Bruger
    {
        public string Brugernavn { get; set; }
        public string NavRolle { get; set; }
    }
}
