﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Navigation;
using System.Collections;
using NLog;

namespace EV.SPEntity
{
    /// <summary>
    /// Wrapper object til SPWeb for entitysite
    /// </summary>
// ReSharper disable once InconsistentNaming
    public class SPEntityWeb : IDisposable
    {
        public const string Issiteproperty = "ev_entitysite";
        public const string Sagsnrproperty = "ev_sagsnr";
        public const string Companyproperty = "ev_company";
        public const string Templateproperty = "ev_template";

        private SPWeb _web;
        public SPWeb Web
        {
            get => _web;
            private set => _web = value;
        }

        public string Title => _web.Title;

        public string Description => _web.Description;

        public SPNavigation Navigation => _web.Navigation;

        public SPEntityWeb()
        { }

        public SPEntityWeb(SPWeb inweb)
        {
            if (!inweb.Properties.ContainsKey(Issiteproperty))
                throw new Exception("Den valgte site er ikke en entity site");
            
            Web = inweb;            
        }

        public SPEntityWeb(string url)
        {
            SPWeb inweb = new SPSite(url).OpenWeb();
            if (!inweb.Properties.ContainsKey(Issiteproperty))
                throw new Exception("Den valgte site er ikke en entity site");

            Web = inweb;
        }
        

        /// <summary>
        /// Opretter foldere på et site
        /// </summary>
        /// <param name="documentlibname"></param>
        /// <param name="folders"></param>
        public void CreateFolders(string documentlibname, string[] folders)
        {
            if (folders == null || !folders.Any())
                throw new Exception("folders variablen må ikke være tom.");

            SPList list;
            try
            {
                list = _web.Lists[documentlibname];
                if (list == null)
                    throw new Exception($"Dokument bibliotket ({documentlibname}) kunne ikke findes på sitets");
            }
            catch (Exception)
            {
                //throw new Exception(string.Format("Dokument bibliotket ({0}) kunne ikke findes på sitets", documentlibname), ex);
                var guid = _web.Lists.Add(documentlibname, documentlibname, SPListTemplateType.DocumentLibrary);
                list = _web.Lists[guid];
            }

            if (list.BaseType != SPBaseType.DocumentLibrary)
                throw new Exception($"Listen '{documentlibname}' er ikke et dokumentbibliotek!");

            foreach (var folder in folders)
            {
                var nyMappe = folder.Replace("/", "\\");
                list.RootFolder.SubFolders.Add(nyMappe);
            }
            list.Update();
        }

        /// <summary>
        /// Retunerer alle items i et niveau. Først foldere så filer
        /// </summary>
        /// <param name="siterelativeurl"> f.eks "/Dokumenter"</param>
        /// <returns></returns>
        public List<DocListItem> GetFolder(string siterelativeurl)
        {
            var folder = _web.GetFolder(siterelativeurl);

            //folder.
            var items = (from SPFolder subfolder in folder.SubFolders
                         where subfolder.Name != "Forms"
                         select new DocListItem
                                    {
                                        ItemType = DocListItem.ItemTypes.Folder, Name = subfolder.Name, Id = subfolder.UniqueId, Url = _web.Url + "/" + subfolder.Url
                                    }).ToList();
            items.AddRange(from SPFile file in folder.Files
                           select new DocListItem
                                      {
                                          ItemType = DocListItem.ItemTypes.File, Name = file.Name, Id = file.UniqueId, Url = _web.Url + "/" + file.Url, IconName = file.IconUrl.EndsWith(".gif", StringComparison.CurrentCultureIgnoreCase) ? file.IconUrl.ToLower().Replace(".gif", ".png") : file.IconUrl
                                      });

            return items;            
        }


        public void AddUser(string loginName, EvspRoleType rolletype)
        {
            _web.AllowUnsafeUpdates = true;
            _web.EnsureUser(loginName);

            var assignment = new SPRoleAssignment(loginName, string.Empty, string.Empty, string.Empty);
            assignment.RoleDefinitionBindings.Add(_web.RoleDefinitions.GetByType(ConvertRoleType(rolletype)));
            //assignment.RoleDefinitionBindings.Add(web.RoleDefinitions.GetByType(SPRoleType.
            _web.RoleAssignments.Add(assignment);            
            _web.Update();
            _web.AllowUnsafeUpdates = false;
        }

        private static SPRoleType ConvertRoleType(EvspRoleType rolletype)
        {
            switch (rolletype)
            {
                case EvspRoleType.Admin:
                    return SPRoleType.Administrator;                    
                case EvspRoleType.Guest:
                    return SPRoleType.Reader;
                    //return SPRoleType.Guest;
                case EvspRoleType.Member:
                    return SPRoleType.Contributor;
            }
            return SPRoleType.None;
        }

        public void RemoveUser(string loginName)
        {
            _web.AllowUnsafeUpdates = true;

            SPUser user = _web.EnsureUser(loginName);

            _web.RoleAssignments.Remove(user);            
            _web.Update();
            _web.AllowUnsafeUpdates = false;
        }

        public void ClearEvProperties()
        {
            clearEVProperties(_web);
        }

// ReSharper disable once InconsistentNaming
        public static void clearEVProperties(SPWeb inWeb)
        {
            var temp = (from DictionaryEntry prop in inWeb.Properties where prop.Key.ToString().ToLower().StartsWith("ev_") select prop.Key.ToString()).ToList();

            if (temp.Count > 0)
            {
                foreach (string k in temp)
                    inWeb.Properties.Remove(k);

                inWeb.Properties.Update();
            }
        }

        public void AddProperty(string key, string value)
        {
            //SPSecurity.RunWithElevatedPrivileges(() =>
            //    {
                    //clear

                    if (_web.Properties.ContainsKey(key))
                        _web.Properties[key] = value;
                    else
                        _web.Properties.Add(key, value);
                    _web.Properties.Update();
            //    });
        }

        public void SetProperty(string key, string value)
        {
            if (_web.Properties.ContainsKey(key))
            {
                _web.Properties[key] = value;
                _web.Properties.Update();
            }
        }

        public string GetProperty(string key)
        {
            if (!_web.Properties.ContainsKey(key))
                throw new Exception("Sitet indeholder ikke propertien: "+key);

            return _web.Properties[key];
        }

        public void RemoveProperty(string key)
        {
            if (_web.Properties.ContainsKey(key))
            {
                _web.Properties.Remove(key);
                _web.Properties.Update();
            }
        }

        public Dictionary<string,string> GetProperties()
        {
            return _web.Properties.Cast<DictionaryEntry>().ToDictionary(prop => (string) prop.Key, prop => (string) prop.Value);
        }

        public void SetTitle(string title)
        {
            var resource = _web.TitleResource;
            foreach (var culture in _web.SupportedUICultures)
            {
                Thread.CurrentThread.CurrentUICulture = culture;
                resource.Value = title;
            }
            _web.Update();
        }

        public void SetDescription(string description)
        {
            var resource = _web.DescriptionResource;
            foreach (var culture in _web.SupportedUICultures)
            {
                Thread.CurrentThread.CurrentUICulture = culture;
                resource.Value = description;
            }
            _web.Update();
        }

        public string GetUrl()
        {
            return Web.Url;
        }

        /// <summary>
        /// Tjekker web for dokumenter. Hvis dokumentbiblioteket ikke indeholder dokumenter slettets webbet.
        /// </summary>
        /// <param name="defaultdoclib">Dokumentbibliotek der skal tjekkes.</param>
        public void DeleteAndCheckForDocuments(string defaultdoclib)
        {
            //TODO: Check om ItemCount inkluderer foldere.
            var doclib = (SPDocumentLibrary)_web.Lists[defaultdoclib];
            if (doclib.ItemCount != 0)
                _web.Delete();
            else
                throw new Exception("Webstedet indeholder dokumenter og kan derfor ikke slettes");
        }

        #region Static

        /// <summary>
        /// Tjekker om sitet er et entity site
        /// </summary>
        /// <param name="web"></param>
        /// <returns></returns>
        public static bool IsEntitySite(SPWeb web)
        {
            if (web.Properties.ContainsKey(Issiteproperty))
                return true;
            return false;
        }

        /// <summary>
        /// Opretter et entity-site og returnerer komplet url på sitet
        /// </summary>
        /// <param name="templatename">template navn til det nye site. Custom templates skal ende på ".stp"</param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="rootUrl"></param>
        /// <param name="subUrl"></param>
        /// <param name="languageid"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static string CreateEntityWeb(string templatename, string title, string description, string rootUrl, string subUrl, uint languageid, Logger logger)
        {
            if (string.IsNullOrEmpty(templatename)) throw new Exception("Der er ikke angivet en template til sitet");
            if (string.IsNullOrEmpty(title)) throw new Exception("Der er ikke angivet en titel til sitet");
            if (string.IsNullOrEmpty(rootUrl)) throw new Exception("Der er ikke angivet en siteUrl (rootUrl)");
            if (string.IsNullOrEmpty(subUrl)) throw new Exception("Der er ikke angivet en sub-url til entitystes");

            //logger.Info("CE - Step 1");

            if (!rootUrl.EndsWith("/"))
                rootUrl = string.Concat(rootUrl, "/");

            if (subUrl.StartsWith("/"))
                subUrl = subUrl.TrimStart('/');

            string completeUrl = rootUrl + subUrl;

            //Get sub level names
            var subLevels = subUrl.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var sagssite = subLevels.Last();
            subLevels.RemoveAt(subLevels.Count - 1);

            //Create RootWebUrl            
            var rootWebUrl = rootUrl.TrimEnd('/'); //remove trailing "/"
            rootWebUrl = subLevels.Aggregate(rootWebUrl, (current, lvl) => string.Concat(current, "/", lvl));

            //logger.Info(String.Format("CE - STep 2 [{0}]", completeUrl));

            using (var site = new SPSite(completeUrl))
            {
                string subcontainers  = rootWebUrl.ToUpper().Replace(site.Url.ToUpper(), "");
                //bool isFirstLevelSiteCollection = false;

                //check om første level er en site colletion
                //hvis første level er en sitecollection skal vi skippe den web
                //if (site.Url.ToUpper().EndsWith(subLevels.First().ToUpper()))
                //{
                //    isFirstLevelSiteCollection = true;
                //    subLevels.RemoveAt(0);
                //    siteRelativeUrl = "";
                //    foreach (var lvl in subLevels)
                //        siteRelativeUrl = siteRelativeUrl + "/" + lvl;
                //    siteRelativeUrl = siteRelativeUrl.TrimStart(new char[] { '/' });
                //}

                //Valider sub levels
                //ValidateSubLevels(site.Url, subLevels);
                //logger.Info(String.Format("CE - STep sublevels"));

                ValidateSubLevels(site.Url, subcontainers.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).ToList());
                
                //opret entity sitet
                //using (SPWeb rootWeb = (isFirstLevelSiteCollection == true ? site.OpenWeb(siteRelativeUrl) : site.RootWeb)) // site.OpenWeb(siteRelativeUrl))


                //logger.Info(String.Format("CE - STep 3 [{0}]", rootWebUrl));

                using (var rootWeb = new SPSite(rootWebUrl).OpenWeb())
                {
                    try
                    {
                        //logger.Info(String.Format("CE - STep 4"));
                        //Tjek site templates først
                        var templates = site.GetWebTemplates(languageid).OfType<SPWebTemplate>();
                        // Name propery has changed in SP 2010!!! Use Title Instead..
                        var templatename1 = templatename;
                        var stdMatches = templates.Where(s => s.Title.ToUpper() == templatename1.ToUpper());
                        var spWebTemplates = stdMatches as SPWebTemplate[] ?? stdMatches.ToArray();
                        if (spWebTemplates.Length != 1)
                        {
                            //Tjek Custom Templates
                            var customTemplates = site.GetCustomWebTemplates(languageid).OfType<SPWebTemplate>();
                            // Name propery has changed in SP 2010!!! Use Title Instead..
                            var templatename2 = templatename;
                            var customMatches = customTemplates.Where(s => s.Title.ToUpper() == templatename2.ToUpper());
                            var webTemplates = customMatches as SPWebTemplate[] ?? customMatches.ToArray();
                            if (webTemplates.Length != 1)
                                throw new Exception($"Der kunne ikke findes en entydig site template i {site.Url} med navnet: {templatename}, LanID:{languageid}");
                            templatename = webTemplates.ToArray()[0].Name;
                        }
                        else
                            templatename = spWebTemplates[0].Name;

                        //logger.Info(String.Format("CE - STep 5"));

                        var allowUnsafeupdates = rootWeb.AllowUnsafeUpdates;
                        rootWeb.AllowUnsafeUpdates = true;

                        string returl;

                        using (var newweb = rootWeb.Webs.Add(sagssite, title, description, languageid, templatename, true, false))
                        {

                            //logger.Info(String.Format("CE - STep 6"));

                            clearEVProperties(newweb);
                            //if (newweb.Properties.ContainsKey(ISSITEPROPERTY))
                            //    newweb.Properties[ISSITEPROPERTY] = "true";
                            //else
                            newweb.Properties.Add(Issiteproperty, "true");
                            newweb.Properties.Update();

                            rootWeb.AllowUnsafeUpdates = allowUnsafeupdates;

                            returl = newweb.Url;
                        }

                        //logger.Info(String.Format("CE - STep 7 [{0}]", returl));
                        return returl;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }

                }
            }          
        }

        private static void ValidateSubLevels(string rootUrl, List<string> subLevels)
        {
            if (subLevels.Count <= 0) return;

            var newRoot = rootUrl + "/" + subLevels.First();

            using (var rootWeb = new SPSite(rootUrl).OpenWeb())
            {                    
                if (!rootWeb.Webs.Names.Contains(subLevels.First(), StringComparer.CurrentCultureIgnoreCase))
                {
                    //createWeb 
                    var web = rootWeb.Webs.Add(subLevels.First());
                    web.Title = subLevels.First();
                    web.Update();
                    web.Dispose();
                }

                subLevels.RemoveAt(0);

                if (subLevels.Count > 0)
                {
                    ValidateSubLevels(newRoot, subLevels);
                }
            }
        }
        
        /*
        private static void GetWebtemplate(SPSite site, string templatename, uint lang)
        {
            //Tjek site templates først
            var templates = site.GetWebTemplates(lang).OfType<SPWebTemplate>();
            var stdMatches = templates.Where(s => s.Name.ToUpper() == templatename.ToUpper());
            if (stdMatches.Count() == 1)
                return;

            //Tjek Custom Templates
            var customTemplates = site.GetCustomWebTemplates(lang).OfType<SPWebTemplate>();
            var customMatches = customTemplates.Where(s => s.Name.ToUpper() == templatename.ToUpper());
            if (customMatches.Count() == 1)
// ReSharper disable RedundantJumpStatement
                return;
// ReSharper restore RedundantJumpStatement
            throw new Exception(string.Format("Der kunne ikke findes en entydig site template i {0} med navnet: {1}, LanID:{2}",site.Url, templatename, lang));
        }
        */

        public static bool SiteExists(string url)
        {
            try
            {
                using (var site = new SPSite(url))
                {
                    using (site.OpenWeb(url, true))
                    {
                        return true;
                    }
                }
            }
            catch (FileNotFoundException)
            {
                return false;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Web?.Dispose();
        }

        #endregion

        
    }

    public enum EvspRoleType
    {
        Admin,
        Guest,
        Member
    }
}
